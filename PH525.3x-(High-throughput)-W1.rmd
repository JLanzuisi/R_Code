---
title: "PH525.3x (High-throughput) W1"
author: "jhonny"
date: "8/4/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


filename GSM136727.CEL.gz	

```{r}
library(devtools)
library(dplyr)
install_github("genomicsclass/GSE5859Subset")
library(GSE5859Subset)
data(GSE5859Subset) ##this loads the three tables

head(sampleInfo$date == "2005-06-27")
sum(sampleInfo$date == "2005-06-27")

newdata <- filter(geneAnnotation, SYMBOL=="ARPC1A")

sum(geneAnnotation$SYMBOL == "ARPC1A")
head(geneExpression)
sum(is.na(geneAnnotation$SYMBOL))



filter(geneExpression, )
```

